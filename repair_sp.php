

<?php

// need the Settings.php info for database stuff.
// require_once( dirname( __FILE__ ) . '/Settings.php' ); // cant from localhost
require_once( 'Settings.php' );



$conn = new mysqli($db_server, $db_user, $db_passwd, $db_name);
if (!$conn) {
    die( 'Connection failed: ' . mysqli_connect_error() );
}


    $CONFIG = array(

    'oldUrl' => 'http://www.test.org/', // require '/' at end
    'oldUrl_PHP' => '/http:\/\/www\.test\.org\/', // require '/' at start and end - todo genarate by function
    'newUrl' => 'http://forum.test.org/', // todo - get host
    'language' => 'de', // Set Language : "de" (german) or "en" (english)

    // todo - all threads that NOT search and NOT upgrade
    'excludeThreads' => array(
        216364
        // 34889
    )

);



    $links = array(
        /**
         * $oldUrl = '/http:\/\/www\.test\.org\/';
         * $fromUrlSearch = '[fF]orum\.([0-9]{1,})\.php/';
         * $suchmuster = $oldUrl . $fromUrlSearch;
         * $ersetzung = '<b>id $2</b>';
         * $zeichenkette = 'sadf http://www.test.org/forum.123.php asdf http://www.test.org/forum.php 321';
         *
         * echo preg_replace(
         * $suchmuster,
         * $ersetzung,
         * $zeichenkette
         * );
         */
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum.html',
            'fromUrl_PHP' => '[fF]orum\.html/',
            'toUrl_PHP' => 'index.php'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-do_unread.html',
            'fromUrl_PHP' => '[fF]orum-action-do_unread\.html/',
            'toUrl_PHP' => 'index.php?action=unread'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help.html',
            'fromUrl_PHP' => '[fF]orum-action-help\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help-page-post.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-post\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help-page-post.html#bbc',
            'fromUrl_PHP' => '[fF]orum-action-help-page-post\.html#bbc/',
            'toUrl_PHP' => 'index.php?action=help',
            '_example' => array(
                'fromUrl Forum-action-help-page-post.html#bbc'
            )
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help-page-profile.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-profile\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help-page-searching.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-searching\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-pm.html',
            'fromUrl_PHP' => '[fF]orum-action-pm\.html/',
            'toUrl_PHP' => 'index.php?action=pm'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-profile.html',
            'fromUrl_PHP' => '[fF]orum-action-profile\.html/',
            'toUrl_PHP' => 'index.php?action=profile'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-profile-u-[0-9]{1,}-sa-showPosts.html',
            'fromUrl_PHP' => '[fF]orum-action-profile-u-([0-9]{1,})-sa-showPosts\.html/',
            'toUrl_PHP' => 'index.php?action=profile;area=showposts;u=$1'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-board-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-board-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?board=$1.0',
            '_example' => array(
                'fromUrl forum-board-166.html'
            )
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-profil-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-profil-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?action=profile;u=$1'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-topic-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?topic=$1.0'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}.[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}.[0-9]{1,}\.html/',
            'toUrl_PHP' => null
        ),
        array(
            // todo check
            'active' => true,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}-start-msg[0-9]{1,}.html[#msg[0-9]{0,}',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}-start-msg[0-9]{1,}\.html([#msg[0-9]{0,})/',
            'toUrl_PHP' => 'index.php?topic=$1.msg$2$3',
            '_example' => array(
                'fromUrl Forum-topic-24375-start-msg162986.html',
                'fromUrl Forum-topic-24529-start-msg164271.html#msg164271',
                'toUrl index.php?topic=19258.msg134131',
                'toUrl index.php?topic=19258.msg134131#msg134131'
            )
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}-start-new-topicseen.html#new',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}-start-new-topicseen\.html#new/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules/Forum/smf/Themes/[a-zA-Z0-9]{1,}/images/[a-zA-Z]{1,}\/[a-zA-Z0-9]{1,}.(gif|png|jpg|jpeg)',
            'fromUrl_PHP' => 'modules\/Forum\/smf\/Themes\/([a-zA-Z0-9]{1,})\/images\/([a-zA-Z]{1,})\/([a-zA-Z0-9]{1,})\.(gif|png|jpg|jpeg)/',
            'toUrl_PHP' => 'Themes/$1/images/$2/$3.$4'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)action=search',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)action=search/',
            'toUrl_PHP' => 'index.php?action=search'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)action=post;board=[0-9]{1,}.[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)action=post;board=([0-9]{1,})\.([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)topic=[0-9]{1,}.[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules\.php?name=Forum(&amp;|&)topic=[0-9]{1,}.msg[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.msg([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => true,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)topic=[0-9]{1,}.msg[0-9]{1,}#msg[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.msg([0-9]{1,})#msg([0-9]{1,})/',
            'toUrl_PHP' => null,
            // todo         => 'index.php?topic={XXX}.msg{XXX}#msg{XXX}'
        )
    );


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1"/>
    <title>TITLE</title>
    <link rel="stylesheet" type="text/css" href="Themes/default/css/index.css?fin20" />
    <link rel="stylesheet" type="text/css" href="Themes/default/css/install.css?fin20" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <style></style>
    <script type="text/javascript">
        $.urlParam = function() {
            var results = new RegExp('[\?&]step=([^&#]*)').exec( window.location.href );
            return results === null ? null : results[1] || null;
        };
        $.setProcessPercent = function( percent_step, percent_1 ) {
            var id_0 = Math.round( percent_1 / percent_step );
            var id_1 = Math.round( percent_1 * 100 ) / 100;
            var id_1_width = Math.round( percent_1 );
            $( "#mx-process-0" ).html( id_0 + "&nbsp;%" );
            $( "#mx-process-width-0" ).width( id_0 + "%" );
            $( "#mx-process-1" ).html( id_1 + '&nbsp;%' );
            $( "#mx-process-width-1" ).width( id_1_width + "%" );
        };
    </script>
</head>
<body>
<table border="1" style="padding-top: 5em;">
    <tr>
        <th>Prozess</th>
        <th>Gesamt</th>
    </tr>
    <tr>
        <td>Links</td>
        <td><ol>
                <?php
            foreach ($links as $index => $item) {
                //sleep(2);
                if ($item['active'] === true) {
                    echo '<li>' . $item['fromUrl_DB'] . '<br>';
                }
            }
            ?>
            </ol></td>
    </tr>
    <tr>
        <td>getThreads</td>
        <td><?php

            foreach ($links as $index => $item) {

                if ($item['active'] === true) {

                    $result = $conn->query(
                    /** @lang text */
                        "SELECT id_msg, id_topic, body FROM {$db_prefix}messages WHERE body REGEXP '" . $CONFIG['oldUrl'] . "{$item['fromUrl_DB']}' AND id_msg NOT IN ( '" . implode($CONFIG['excludeThreads'], "', '") . "' )");

                    $count_found_links = $result->num_rows;

                    if ($count_found_links > 0) {
                        echo 'update ';
                        while ($row = $result->fetch_assoc()) {
                            echo '<br>Betrag Nr. ' . $row['id_topic'];
                        }
                    } else {
                        echo 'not, ';
                    }
                }
            }
            ?></td>
    </tr>
    <tr>
        <td>getAllrows</td>
        <td><?php
                // anzahl der zu durchsuchende beiträge
                echo $conn->query(
                    /** @lang text */
                    "SELECT id_msg FROM {$db_prefix}messages")->num_rows;
            ?></td>
    </tr>
</table>
</body>
</html>
