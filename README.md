# 
> TODO - Im Grunde sind das 2 Plugins

> Ändern der geposteten Pfade in den Beiträgen von der alten Domain zur neuen.

## SMF-url-repair-tool
* TODO - Dieses Script hochladen
* TODO - Konfigurieren
* TODO - Ausführen
* TODO - Datei löschen (automatisch per button)
* TODO - pmx Bridge kontrollieren


#### Umziehen eines 'Pragmamx Module Forum SMF' auf eine Sub- oder Domain.
* Schwierigkeit: schwer
* Erforderliche Kenntnisse: FTP, PHP und MySQL.
* INFO: Für SMF-Version '2.0.13' wird eine PHP-Version '<= 5.6.x' benötigt.


## Folgende Dateien downloaden:
1. SMF Forum [Large upgrade](http://download.simplemachines.org)
2. SMF Forum [Repair Settings](http://download.simplemachines.org/?tools) 


## Datenbank
* Die Datenbank vom PMX-Module-SMF-Bridge in eine neue Datenbank kopieren.
* Dann in der neuen Datenbank ausführen:
```
// {prefix} anpassen

UPDATE `{prefix}themes`
	SET `value` = 'Themes/default'
	WHERE `id_member` = 0
	AND `id_theme` = 1
	AND `variable` = 'theme_dir';

UPDATE `{prefix}themes`
	SET `value` = 'Themes/default'
	WHERE `id_member` = 0
	AND `id_theme` = 1
	AND `variable` = 'theme_url';

UPDATE `{prefix}themes`
	SET `value` = 'default/images'
	WHERE `id_member` = 0
	AND `id_theme` = 1
	AND `variable` = 'images_url';

UPDATE `{prefix}settings`
	SET `value` = 'attachments'
	WHERE `variable` = 'attachmentUploadDir';
```

## Installation des Forums
* "Large upgrade" per FTP auf die neue Domain hochladen.
* Ordner und Dateien vom PMX-Forum hinzufügen:
    * ./attachments/* nach ./attachments/
    * ./avatars/* nach avatars/
    * ./Smileys/* nach ./Smileys/
    * ./Settings.php nach ./
        * Die Datenbank Zugangsdaten ändern!
        * "Repair Settings" ausführen (Dabei werden die Einstellungen in der Settings.php überprüft).
	        * 'Set SMF Default theme as overall forum default for all users:' auf 'Yes'
* "./upgrade.php" aufrufen (Damit wird ein neues Forum mit den PMX-SMF-Bridge erstellt).
* Jetzt können weitere Einstellungen zum Forum getroffen werden (Plugins, Themes, Sprachen etc.).


## Information: Datenbank vom Forum
Die Datenbank Tabellen von PMX Module Forum 'SMF v1.1.21' (ohne Prefix) inkl PMX Bridge.
```
_attachments
_ban_groups
_ban_items
_boards
_board_permissions
_calendar
_calendar_holidays
_categories
_collapsed_categories
_log_actions
_log_activity
_log_banned
_log_boards
_log_errors
_log_floodcontrol
_log_karma
_log_mark_read
_log_notify
_log_online
_log_polls
_log_search_messages
_log_search_results
_log_search_subjects
_log_search_topics
_log_topics
_membergroups
_members
_messages
_message_icons
_moderators
_package_servers
_permissions
_personal_messages
_pm_recipients
_polls
_poll_choices
_sessions
_settings
_smileys
_themes
_topics
```
