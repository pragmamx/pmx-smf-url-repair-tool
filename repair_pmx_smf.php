<?php

/**
 * Class RepairPmxSmfForumLinks
 *
 * @class RepairPmxSmfForumLinks
 *
 * @author Sven Hedström-Lang <SvenLang07@gmail.com>
 * @copyright 2017-01-06
 * @since pragmaMx 2.4.0
 * @phpversion >=5.6
 * @require
 *      SMF v2.0.12
 * @include
 *      ./Settings.php
 *      ./Themes/default/css/*
 *      jQuery v3.1.1
 */

/**
 * todo
 *
 * cache leeren
 *
 * fehlende bilder
 * ./index.php?topic=30647.msg195251#msg195251
 * ./index.php?topic=29403.msg188434#msg188434
 *
 * repair anwenden - speichern
 * repair löschen
 * upgrade
 * done
 * Admin Änderungen
 * Language NOT utf-8
 *
 * css extern ?
 * js extern ?
 *
 *
 */

/**
 * Zum Testen:
 * 1. setze 'oldUrl' auf 'http://www.test.org/' und 'newUrl' auf 'http://www.test2.org/'
 * 2. erstelle nachfolgenden neuen Beitrag
 * 3. und starte das Script
 *
http://www.test.org/forum.html
http://www.test.org/Forum.html
http://www.test.org/forum-action-do_unread.html
http://www.test.org/Forum-action-do_unread.html
http://www.test.org/forum-action-help.html
http://www.test.org/Forum-action-help.html
http://www.test.org/forum-action-help-page-post.html
http://www.test.org/Forum-action-help-page-post.html
http://www.test.org/forum-action-help-page-post.html#bbc
http://www.test.org/Forum-action-help-page-post.html#bbc
http://www.test.org/forum-action-help-page-profile.html
http://www.test.org/forum-action-help-page-searching.html
http://www.test.org/forum-action-pm.html
http://www.test.org/forum-action-profile.html
http://www.test.org/forum-action-profile-u-{XXX}-sa-showPosts.html
http://www.test.org/forum-board-166.html
http://www.test.org/forum-profil-{XXX}.html
http://www.test.org/forum-topic-{XXX}.html
http://www.test.org/forum-topic-{XXX}.{XXX}.html
http://www.test.org/Forum-topic-24375-start-msg162986.html
http://www.test.org/Forum-topic-24375-start-msg162986.html#msg162986
http://www.test.org/forum-topic-{XXX}-start-new-topicseen.html#new
http://www.test.org/index.php?topic={XXX}.msg{XXX}#msg{XXX}
http://www.test.org/modules/Forum/smf/Themes/{XXX}/images/{XXX}/{XXX}.gif
http://www.test.org/modules.php?name=Forum
http://www.test.org/modules.php?name=Forum&amp;action=search
http://www.test.org/modules.php?name=Forum&amp;action=post;board={XXX}.{XXX}
http://www.test.org/modules.php?name=Forum&amp;topic={XXX}.{XXX}
http://www.test.org/modules.php?name=Forum&amp;topic={XXX}.msg{XXX}
http://www.test.org/modules.php?name=Forum&amp;topic={XXX}.msg{XXX}#msg{XXX}
http://www.test.org/forum-
 *
 */


// need the Settings.php info for database stuff.
// require_once( dirname( __FILE__ ) . '/Settings.php' ); // cant from localhost
require_once( 'Settings.php' );

class RepairPmxSmfForumLinks
{

    // ################
    // # START CONFIG #
    // ################

    const CONFIG = array(
        //'oldUrl'         => 'http://www.test.org/', // require '/' at end
        //'oldUrl_PHP'     => '/http:\/\/www\.test\.org\/', // require '/' at start and end - todo genarate by function

        'oldUrl' => 'http://www.test.org/',
        'oldUrl_PHP' => '/http:\/\/www\.test\.org\/',
        'newUrl' => 'http://forum.test.org/', // todo - get host
        'language' => 'de', // Set Language : "de" (german) or "en" (english)

        // todo - all threads that NOT search and NOT upgrade
        'excludeThreads' => array(
             216364,
            7,
            8
            // 34889
        )

    );

    // ##############
    // # END CONFIG #
    // ##############

    const DEFAULT_LANGUAGE = 'de';

    /**
     * The table name to search.
     *
     * todo rename for release
     */
    const SEARCH_DB = 'messages2';

    /**
     * @type int
     * @default 1
     */
    const DEFAULT_STEP = 1;

    /**
     * The Database reference.
     *
     * @default null
     */
    private $_conn = null;

    /**
     * todo - delete ?
     *
     * @var array
     */
    private $_report = array();

    public function __construct()
    {
        $this->_setStartPercent();
        $this->_getTemplate($this->_getStep() - 1);
        $this->_setMainScreenTemplate($this->_getStep() - 1);
    }

    /**
     * @var array - todo new documentation
     *
     * @param bool [][active]
     * @param string [][fromUrl] - The display for user in HTML
     * @param string [][fromUrlSearch] - The regex to search with MySQL - todo delete
     * @param string [][toUrl] - The display for user in HTML
     * @param string [][toUrlReplace] - The regex to replace with PHP
     */
    private $_urls = array(
        /**
         * $oldUrl = '/http:\/\/www\.test\.org\/';
         * $fromUrlSearch = '[fF]orum\.([0-9]{1,})\.php/';
         * $suchmuster = $oldUrl . $fromUrlSearch;
         * $ersetzung = '<b>id $2</b>';
         * $zeichenkette = 'sadf http://www.test.org/forum.123.php asdf http://www.test.org/forum.php 321';
         *
         * echo preg_replace(
         * $suchmuster,
         * $ersetzung,
         * $zeichenkette
         * );
         */
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum.html',
            'fromUrl_PHP' => '[fF]orum\.html/',
            'toUrl_PHP' => 'index.php'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-do_unread.html',
            'fromUrl_PHP' => '[fF]orum-action-do_unread\.html/',
            'toUrl_PHP' => 'index.php?action=unread'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-help.html',
            'fromUrl_PHP' => '[fF]orum-action-help\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-help-page-post.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-post\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-help-page-post.html#bbc',
            'fromUrl_PHP' => '[fF]orum-action-help-page-post\.html#bbc/',
            'toUrl_PHP' => 'index.php?action=help',
            '_example' => array(
                'fromUrl Forum-action-help-page-post.html#bbc'
            )
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-help-page-profile.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-profile\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-help-page-searching.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-searching\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-pm.html',
            'fromUrl_PHP' => '[fF]orum-action-pm\.html/',
            'toUrl_PHP' => 'index.php?action=pm'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-profile.html',
            'fromUrl_PHP' => '[fF]orum-action-profile\.html/',
            'toUrl_PHP' => 'index.php?action=profile'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-action-profile-u-[0-9]{1,}-sa-showPosts.html',
            'fromUrl_PHP' => '[fF]orum-action-profile-u-([0-9]{1,})-sa-showPosts\.html/',
            'toUrl_PHP' => 'index.php?action=profile;area=showposts;u=$1'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-board-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-board-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?board=$1.0',
            '_example' => array(
                'fromUrl forum-board-166.html'
            )
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-profil-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-profil-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?action=profile;u=$1'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-topic-([0-9]{1,})\.html/',
            'toUrl_PHP' => 'index.php?topic=$1.0'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}.[0-9]{1,}.html',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}.[0-9]{1,}\.html/',
            'toUrl_PHP' => null
        ),
        array(
            // todo check
            'active' => false,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}-start-msg[0-9]{1,}.html[#msg[0-9]{0,}',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}-start-msg[0-9]{1,}\.html([#msg[0-9]{0,})/',
            'toUrl_PHP' => 'index.php?topic=$1.msg$2$3',
            '_example' => array(
                'fromUrl Forum-topic-24375-start-msg162986.html',
                'fromUrl Forum-topic-24529-start-msg164271.html#msg164271',
                'toUrl index.php?topic=19258.msg134131',
                'toUrl index.php?topic=19258.msg134131#msg134131'
            )
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-topic-[0-9]{1,}-start-new-topicseen.html#new',
            'fromUrl_PHP' => '[fF]orum-topic-[0-9]{1,}-start-new-topicseen\.html#new/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules/Forum/smf/Themes/[a-zA-Z0-9]{1,}/images/[a-zA-Z]{1,}\/[a-zA-Z0-9]{1,}.(gif|png|jpg|jpeg)',
            'fromUrl_PHP' => 'modules\/Forum\/smf\/Themes\/([a-zA-Z0-9]{1,})\/images\/([a-zA-Z]{1,})\/([a-zA-Z0-9]{1,})\.(gif|png|jpg|jpeg)/',
            'toUrl_PHP' => 'Themes/$1/images/$2/$3.$4'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)action=search',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)action=search/',
            'toUrl_PHP' => 'index.php?action=search'
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)action=post;board=[0-9]{1,}.[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)action=post;board=([0-9]{1,})\.([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)topic=[0-9]{1,}.[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules\.php?name=Forum(&amp;|&)topic=[0-9]{1,}.msg[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.msg([0-9]{1,})/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => false,
            'fromUrl_DB' => 'modules.php?name=Forum(&amp;|&)topic=[0-9]{1,}.msg[0-9]{1,}#msg[0-9]{1,}',
            'fromUrl_PHP' => 'modules\.php\?name=Forum(&amp;|&)topic=([0-9]{1,})\.msg([0-9]{1,})#msg([0-9]{1,})/',
            'toUrl_PHP' => null,
            // todo         => 'index.php?topic={XXX}.msg{XXX}#msg{XXX}'
        )
    );

    private $_controll_urls = array(

        // todo - for control -> all other missing in default array - or set manual

        array(
            'active' => false,
            'fromUrl_DB' => 'modules.php?name=Forum',
            'fromUrl_PHP' => 'modules\.php\?name=Forum/',
            'toUrl_PHP' => null
        ),
        array(
            'active' => false,
            'fromUrl_DB' => '[fF]orum-',
            'fromUrl_PHP' => '[fF]orum-/',
            'toUrl_PHP' => null
        )

    );

    private $_translate = array(
        'find_threads' => array(
            'de' => 'Gefundene Links:',
            'en' => 'Links found:'
        ),
        'form_check' => array(
            'de' => 'Kontrolle',
            'en' => 'Check'
        ),
        'form_done' => array(
            'de' => 'Fertig',
            'en' => 'Done'
        ),
        'form_first' => array(
            'de' => 'Anleitung',
            'en' => 'Readme'
        ),
        'FORM_SEARCH_AND_REPLACE' => array(
            'de' => 'Suchen und ersetzen',
            'en' => 'Search and replace'
        ),
        'form_submit' => array(
            'de' => 'Ausführen',
            'en' => 'Submit'
        ),
        'NEW' => array(
            'de' => 'Neu',
            'en' => 'New'
        ),
        'TITLE' => array(
            'de' => 'PragmaMX Modules Forum/SMF Link Reparatur Script',
            'en' => 'PragmaMX Modules Forum/SMF Link Repair Tool'
        ),
        'upgrade_progress_title' => array(
            // 'status_process' - is the same, but is the status text
            'de' => 'Prozess',
            'en' => 'Process'
        ),
        'status' => array(
            'de' => 'Gesamt',
            'en' => 'Total'
        ),
        'status_process' => array(
            // see 'upgrade_progress_title'
            'de' => 'Prozess',
            'en' => 'Process'
        ),
        '0_title' => array(
            'de' => 'Bitte beachten:',
            'en' => 'Please read:'
        ),
        '0_description' => array(
            // todo - Einstellungen (in der Datei ?
            'de' => array(
                '<li>Einstellungen (in der Datei):',
                '<ul>',
                '<li>Die alte gegen die neue URL austauschen.</li>',
                '</ul>',
                '</li>',
                '<li>Kann nicht rückgängig gemacht werden, ausser durch ein Backup ... also besser vorher ein Backup anlegen.</li>',
                '<li>PragmaMX übernimmt keine Verantwortung.</li>',
                '<li>Dieses Script nicht unterbrechen !!! Es dauert eine Weile ...</li>',
                '<li>Es werden nur die Beiträge durchsucht.</li>',
                '<li>Zum vorrigen Testen kann folgendes getan werden:',
                '<ul>',
                '<li>blah blub</li>',
                '<pre></pre>',
                '</ul></li>'
            ),
            'en' => '' // todo
        ),
        '1_title' => array(
            'de' => 'Folgende Links werden für das SMF Forum angepasst:',
            'en' => '' // todo
        ),
        'error' => array(
            // todo
            1016 => 'Can"t open file',
            1050 => 'Table already exists.',
            1054 => 'Unknown column name.',
            1060 => 'Duplicate column name.',
            1061 => 'Duplicate key name.',
            1062 => 'Duplicate entry for unique key.',
            1068 => 'Multiple primary keys.',
            1072 => 'Key column "%s" doesn"t exist in table.',
            1091 => 'Can"t drop key, doesn"t exist.',
            1146 => 'Table doesn"t exist.',
            2013 => 'Lost connection to server during query.'
        )
    );

    private $_steps = array(
        array(
            'title' => 'form_first'
        ),
        array(
            'title' => 'FORM_SEARCH_AND_REPLACE',
        ),
        array(
            'title' => 'form_check'
        ),
        array(
            'title' => 'form_done'
        )

    );

    private $_process = array(
        array(
            'percent' => 0,
            'color' => 'lime',
            'progressName' => 'status'
        ),
        array(
            'percent' => 0,
            'color' => '#ffd000',
            'progressName' => 'status_process'
        )
        /*,
        todo
            array(
                'percent'      => 0,
                'color'        => '#eebaf4',
                'progressName' => 'link_progress'
            )
        */
    );

    private function _setStartPercent()
    {
        // todo rename
        /*
        <div id="mx-process-text-' . $index . '" class="mx-process-text">' . $item['percent'] . ' %</div>
                <div id="mx-process-width-' . $index . '" class="mx-process-width">&nbsp;</div>
                <div class="progress">' . $this->_( $item['progressName'] ) . '</div>
        */


        // todo - ok ?
    }

    private function _getTemplate($step)
    {
        echo '<!DOCTYPE html>
		<head>
			<meta charset="utf-8" />
			<meta name="robots" content="noindex" />
			<meta http-equiv="cache-control" content="max-age=0" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
			<meta http-equiv="pragma" content="no-cache" />
			<title>' . $this->_('TITLE') . '</title>
			<link rel="stylesheet" type="text/css" href="Themes/default/css/index.css?fin20" />
			<link rel="stylesheet" type="text/css" href="Themes/default/css/install.css?fin20" />
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<style>
				.mx-error {
					/* todo - check - not need */
					color: #b81900;
				}
				.mx-process-wrapper {
					float: left;
					width: 40%;
				}
				.mx-process-wrapper-li {
					background-color: white;
					border: 1px solid black;
					font-size: 8pt;
					height: 12pt;
					margin: 5px auto;
					width: 50%;
				}
				.mx-process-text {
					color: #000;
					margin-left: -5em;
					position: absolute;
				}
				.mx-process-width {
					background-color: black;
					height: 12pt;
					z-index: 1;	
				}
				input[type="button"] {
					padding: 1ex;
					text-transform: uppercase;
				}
				.mx-padding {
					padding: 5ex;
				}
			</style>
			<script type="text/javascript">
				$.urlParam = function() {
					var results = new RegExp(\'[\?&]step=([^&#]*)\').exec( window.location.href );
					return results === null ? null : results[1] || null;
				};
				$.setProcessPercent = function( percent_step, percent_1 ) {
					var id_0 = Math.round( percent_1 / percent_step );
					var id_1 = Math.round( percent_1 * 100 ) / 100;
					var id_1_width = Math.round( percent_1 );
					$( \'#mx-process-0\' ).html( id_0 + \' %\' );
					$( \'#mx-process-width-0\' ).width( id_0 + \'%\' );
					$( \'#mx-process-1\' ).html( id_1 + \' %\' );
					$( \'#mx-process-width-1\' ).width( id_1_width + \'%\' );
				};
			</script>
		</head>
		<body>
			<div id="header"><div class="frame">
				<div id="top_section">
					<h1 class="forumtitle">' . $this->_('TITLE') . '</h1>
				</div>
				<div id="upper_section" class="middletext flow_hidden">
					<div class="user"></div>
					<div class="news normaltext"></div>
				</div>
			</div></div>
			
			<div id="content_section"><div class="frame">
				<div id="main_content_section">
					' . $this->_getProcessTemplate($step) . '
					' . $this->_getStepTemplate($step) . '
					<div class="mx-padding">&nbsp;</div>
					' . $this->_getButtonTemplate($step) . '
					<div id="main_screen" class="clear">
						<h2 id="mx-main-title"></h2>
						<div class="panel">
							<div style="max-height: 250px; overflow: auto;">
								<div class="user">
									<ol id="mx-main-ul" class="reset greeting"></ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div></div>
			
			<div id="footer_section"><div class="frame" style="height: 40px;">
				<div class="smalltext">
					<a href="http://www.pragmamx.org" title="pragmamx.org" target="_blank" class="new_win">&copy; 2016 PragmaMX Team</a>
				</div>
			</div></div>
			
		</body>
	</html>';
    }

    /**
     * @param int $step
     *
     * @private
     */
    private function _setMainScreenTemplate($step)
    {

        global $db_prefix;
        $percent_step = count($this->_steps) / ($step + 1);
        sleep(3); // wait till template ready // todo check

        switch ($step) {

            case 0:
                echo '<script>
					$.setProcessPercent(' . $percent_step . ', 0);
					$(\'#mx-main-title\').html(\'' . $this->_('0_title') . '\');
					$(\'#mx-main-ul\').append(\'' . $this->_('0_description') . '\');
					$.setProcessPercent(' . $percent_step . ', 100);
				</script>';
                break;

            case 1:
                if ($this->_openDB()) {

                    /**
                     * progressbar mit JavaScript realisiert
                     *
                     * $threads_one_table = 10.000 // durchsuchte rows je tabelle, nachdem es immer die gleiche tabelle ist, ändert sich der wert nicht
                     * anzahl der durchsuchten links = 25
                     * das sind dann = 10.000 * 25 ==> 250.000 durchsuchte rows ==> 100%
                     * das heisst also das bei 25 links ist je link 4% erledigt, denn 4*25 sind 100%
                     *
                     * gesuchter prozentsatz = 10.000 / (250.000 / 100) ==> 4%
                     *
                     * Beispiel
                     * $current_threads = 0
                     *
                     * - 2 gefunden von $threads_one_table
                     * - $threads_points = $threads_one_table / 2
                     * - 1te -> ($current_threads + $threads_points) / $threads_all_table / 100
                     * $current_threads += $threads_points
                     * - 2te -> ($current_threads + $threads_points) / $threads_all_table / 100
                     * $current_threads += $threads_points
                     *
                     * -- 5 gefunden von $threads_one_table
                     * -- $threads_points = $threads_one_table / 5
                     * -- 1ste -> ($current_threads + $threads_points) / $threads_all_table / 100
                     * $current_threads += $threads_points
                     * -- 2ste -> ($current_threads + $threads_points) / $threads_all_table / 100
                     * $current_threads += $threads_points
                     * ...
                     *
                     * --- 0 gefunden von $threads_one_table
                     * $current_threads += $threads_one_table
                     */
                    $result_num_rows = $this->_conn->query("SELECT id_msg FROM {$db_prefix}'" . self::SEARCH_DB . "'");
                    $threads_one_table = $result_num_rows->num_rows; // 10.000 => 4%
                    $threads_all_table = $threads_one_table * count($this->_urls); // 250.000 => 100%
                    $current_threads = 0;
                    foreach ($this->_urls as $index => $item) {
                        if ($item['active'] === true) {
                            $toUrl = is_string($item['toUrl_PHP']) ? self::CONFIG['newUrl'] . $item['toUrl_PHP'] : '<span class="mx-error">TODO</span>'; // todo
                    $sql = '';
                    $sql .= "SELECT `id_msg`, `id_topic`, `body`";
                    $sql .= " FROM `".$db_prefix. self::SEARCH_DB ."`";
                    $sql .= " WHERE `body` REGEXP `" . self::CONFIG['oldUrl'] . $item['fromUrl_DB'] ."`";
                    $sql .= " AND `id_msg` NOT IN ( " . implode(self::CONFIG['excludeThreads'], ", ") . " )";

                            echo $sql;
                            $result = $this->_conn->query($sql);

                            $count_found_links = $result->num_rows;
                            if ($count_found_links > 0) {
                                $li = '<li>' . self::CONFIG['oldUrl'] . $item['fromUrl_DB'] . '<div class="smalltext">&nbsp;&nbsp;-> ' . $toUrl . '<br />&nbsp;&nbsp;' . $this->_('find_threads') . ' (' . $count_found_links . ') -> <span id="mx-span-' . $index . '"></span></div></li>';
                                echo '<script>
									$(\'#mx-main-ul\').append(\'' . $li . '\');
								</script>';
                                while ($row = $result->fetch_assoc()) {
                                    $span = '<a href="index.php?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . '#msg' . $row['id_msg'] . '" target="_blank">' . $row['id_msg'] . '</a>, ';
                                }
                            } else {
                                sleep(2);
                            }
                        } else {
                            sleep(2);
                        }
                    }

                    // set end percent (100%)
                    // todo - translate script - no need !!
                    sleep(2);
                    $this->_closeDB();
                } else {
                    // todo - db error
                }
                break;

            case 2:
                echo '<script>
					$.setProcessPercent(' . $percent_step . ', 0);
					$.setProcessPercent(' . $percent_step . ', 100);
				</script>';
                break;

            case 3:
                echo '<script>
					$.setProcessPercent(' . $percent_step . ', 0);
					$.setProcessPercent(' . $percent_step . ', 100);
				</script>';
                break;

            case 4:
                echo '<script>
					$.setProcessPercent(' . $percent_step . ', 0);
					$(\'#mx-main-title\').html(\'fehler bitte melden\');
					$(\'#mx-main-ul\').append(\'Danke\');
					$.setProcessPercent(1, 100);
				</script>';
                break;

            default:
                // todo
                break;

        }
    }

    /**
     * @param int $step
     *
     * @return string
     * @private
     */
    private function _getStepTemplate($step)
    {
        $li = '';
        $style = 'stepwaiting';
        foreach ($this->_steps as $index => $item) {
            if (isset($step)) {
                if ($step < $index) {
                    $style = 'stepdone';
                } elseif ($step === $index) {
                    $style = 'stepcurrent';
                }
            }
            $li .= '<li class="' . $style . '">' . $this->_($item['title']) . '</li>';
        }

        return '<div id="main-steps">
			<h2>' . $this->_('upgrade_progress_title') . '</h2>
			<ul>
				' . $li . '
			</ul>
		</div>';
    }

    private function _getProcessTemplate($step)
    {

        //$this->_setProcessPercent( 0, 100 / count( $this->_steps ) * ( $step + 1 ) );// todo

        /*
        $procent = count( $this->_steps ) / ( $this->_getStep() + 1 );
        if ( $this->_process[0]['percent'] < $procent ) {
            // todo
            $this->_process[0]['percent'] = $procent;
        }

        */


        switch ($step) {

            case 0:
                //$this->_setProcessPercent( 1, 0 );
                break;

            case 1:
                //$this->_setProcessPercent( 0, 0 ); // todo
                break;
            case 2:
            case 3:
            case 4:
            default:
                // todo
                break;
        }

        $li = '';
        foreach ($this->_process as $index => $item) {
            $li .= '<div class="mx-process-wrapper-li">
				<div class="mx-process-text">' . $this->_($item['progressName']) . '</div>
				<div id="mx-process-width-' . $index . '" class="mx-process-width" style="background-color:' . $item['color'] . ';">&nbsp;</div>
				<div id="mx-process-' . $index . '" class="progress">' . $item['percent'] . ' %</div>
			</div>';
        }

        return '<div class="mx-process-wrapper">
			' . $li . '
			<div>&nbsp;</div>
		</div>';
        /**
         * todo
         * <div class="smalltext" style="padding: 5px; text-align: center;">upgrade_time_elapsed
         * <span id="mins_elapsed">mins</span> upgrade_time_mins
         * <span id="secs_elapsed">seconds</span> upgrade_time_secs
         * </div>
         */
    }

    /**
     * Get the index of step.
     *
     * @return int
     * @private
     */
    private function _getStep()
    {
        global $_GET;
        $step = $_GET['step'];
        if (is_numeric($step) && $step >= 0 && $step <= count($this->_steps)) {
            return $step;
        }

        // else DEFAULT_STEP
        header('Location: ' . $this->_getFileName() . '?step=' . self::DEFAULT_STEP);

        return self::DEFAULT_STEP;
    }

    /**
     * Open Database connection.
     *
     * @return bool - Is connection returns true otherwise false.
     * @private
     */
    private function _openDB()
    {
        global $db_server, $db_user, $db_passwd, $db_name;
        $this->_conn = new mysqli($db_server, $db_user, $db_passwd, $db_name);
        if (!$this->_conn) {
            // die( 'Connection failed: ' . mysqli_connect_error() );
            return false;
        }

        // else { echo "Connected successfully"; }
        return true;
    }

    /**
     * Close Database connection.
     *
     * @private
     */
    private function _closeDB()
    {
        if (!is_null($this->_conn)) {
            $this->_conn->close();
        }
        $this->_conn = null;
    }

    /**
     * Get the current Language translate.
     *
     * @param string $key
     *
     * @return string
     * @private
     */
    private function _($key)
    {
        $lang = self::CONFIG['language'] ? self::CONFIG['language'] : self::DEFAULT_LANGUAGE;
        $value = $this->_translate[$key][$lang];
        if (is_array($value)) {
            $out = '';
            foreach ($value as $item) {
                $out .= $item;
            }

            return $out;
        }

        return $value;
    }

    private function _getButtonTemplate($step)
    {
        // todo - a solution in end of steps - try delete file ? yes !
        $stepCount = count($this->_steps) - 1;
        $stepId = $step === $stepCount ? 1 : ($step + 2);
        $stepTitle = $step === $stepCount ? $this->_('NEW') : $this->_($this->_steps[$this->_getStep()]['title']);

        return '<div class="plainbox">
			<div id="mx-status"></div>
			<input id="mx-button" class="button_submit" type="button" value="' . $stepTitle . '" onclick="window.location.href = 
			\'' . $this->_getFileName() . '?step=' . $stepId . '\';">
		</div>';
    }

    /**
     * Get this filename.
     *
     * @return string
     */
    private function _getFileName()
    {
        global $_SERVER;

        return basename($_SERVER['SCRIPT_FILENAME']);
    }

    /**
     * todo check
     *
     * txt : 'Remove this file. Careful, it may not work in all servers!'
     */
    private function _deleteFile() {
        global $_SERVER, $_GET, $_POST;

        // Https?
        $context['schema'] = isset($_SERVER['HTTPS']) || (isset($_SERVER['REQUEST_SCHEME']) && strtolower($_SERVER['REQUEST_SCHEME']) === 'https') ? 'https' : 'http';

        // This is really quite simple; if ?delete is on the URL, delete the installer...
        if (isset($_GET['delete']) || isset($_POST['remove_file']))
        {
            @unlink(__FILE__);

            // Now just redirect to a blank.gif...
            header('Location: ' . $context['schema'] . '://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) . dirname($_SERVER['PHP_SELF']) . '/index.php');
            exit;
        }
    }

}

new RepairPmxSmfForumLinks();

?>
