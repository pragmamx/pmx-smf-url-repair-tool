<?php


error_reporting(E_ALL);


require_once('Settings.php');


class PmxSmf
{

    // ################
    // # START CONFIG #
    // ################

    const CONFIG = array(
        //'oldUrl'         => 'http://www.test.org/', // require '/' at end
        //'oldUrl_PHP'     => '/http:\/\/www\.test\.org\/', // require '/' at start and end - todo genarate by function

        'oldUrl' => 'http://www.test.org/',
        'oldUrl_PHP' => '/http:\/\/www\.test\.org\/',
        'newUrl' => 'http://forum.test.org/', // todo - get host
        'language' => 'de', // Set Language : "de" (german) or "en" (english)

        // todo - all threads that NOT search and NOT upgrade
        'excludeThreads' => array(
            216364
            // 34889
        )

    );

    // ##############
    // # END CONFIG #
    // ##############

    /**
     * PmxSmf constructor.
     */
    public function __construct()
    {

        $this->_openDB();

        $this->_getTemplate();
        $this->_searchUrls();
    }

    private function _getTemplate()
    {
        header("Expires: Mon, 1 Dec 2003 01:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        @ini_set('auto_detect_line_endings', true);
        @set_time_limit(0);

        echo <<<EOT
<tr><td>$animal[0]</td>
<td>$animal[1]</td>
<td>$animal[2]</td>
<td>$animal[3]</td>
<td>$animal[4]</td>
<td><img width="100px" height="100px" src="$animal[5]" alt="$animal[1]"/></td>
<td>$animal[6]</td>
<td>.$animal[7]</td>
<td><button type="submit" name="selected" value="$animal[0]"/></td></tr>
EOT;
        echo ('<!DOCTYPE html>
		<head>
			<meta charset="utf-8" />
			<meta name="robots" content="noindex, nofollow" />
			<meta http-equiv="cache-control" content="max-age=0" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="Expires" content="-1"/>
			<title>TITLE</title>
			<link rel="stylesheet" type="text/css" href="Themes/default/css/index.css?fin20" />
			<link rel="stylesheet" type="text/css" href="Themes/default/css/install.css?fin20" />
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<style></style>
            <script type="text/javascript">
				$.urlParam = function() {
					var results = new RegExp(\'[\?&]step=([^&#]*)\').exec( window.location.href );
					return results === null ? null : results[1] || null;
				};
				$.setProcessPercent = function( percent_step, percent_1 ) {
					var id_0 = Math.round( percent_1 / percent_step );
					var id_1 = Math.round( percent_1 * 100 ) / 100;
					var id_1_width = Math.round( percent_1 );
					$( "#mx-process-0" ).html( id_0 + "&nbsp;%" );
					$( "#mx-process-width-0" ).width( id_0 + "%" );
					$( "#mx-process-1" ).html( id_1 + \'&nbsp;%\' );
					$( "#mx-process-width-1" ).width( id_1_width + "%" );
				};
			</script>
			</head>
			<body>
<table border="1" style="padding-top: 5em;">
<tr>
<th> </th>
<th>Prozess</th>
<th>Gesamt</th>
</tr>
<tr>
<td>Links</td>
<td id="mx-links-process"></td>
<td></td>
</tr>
<tr>
<td>Beiträge</td>
<td id="mx-threads-process"></td>
<td id="mx-threads-total"></td>
</tr>
</table>
                <div id="mx-percent"></div>
			    <div id="mx-main-title"></div>
			    <ul id="mx-main-ul"></ul>
			</body>
        </html>');
    }

    private $_links = array(
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum.html',
            'fromUrl_PHP' => '[fF]orum\.html/',
            'toUrl_PHP' => 'index.php'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-do_unread.html',
            'fromUrl_PHP' => '[fF]orum-action-do_unread\.html/',
            'toUrl_PHP' => 'index.php?action=unread'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help.html',
            'fromUrl_PHP' => '[fF]orum-action-help\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        ),
        array(
            'active' => true,
            'fromUrl_DB' => '[fF]orum-action-help-page-post.html',
            'fromUrl_PHP' => '[fF]orum-action-help-page-post\.html/',
            'toUrl_PHP' => 'index.php?action=help'
        )
    );
    private function _searchUrls()
    {
        global $db_prefix;


        if ($this->_conn) {
            // search urls
            // list at template
            // add percent ?

            // anzahl der zu suchenden links
            $count_links = count($this->_links);

            // anzahl der zu durchsuchende beiträge
            $result_num_allrows = $this->_conn->query("SELECT id_msg FROM {$db_prefix}messages")->num_rows;

            // gesamt anzahl der zu durchsuchende beiträge
            $sum_all_threads = $count_links * $result_num_allrows;

            echo '<script>
                $("#mx-links-process").html("0 von ' . $count_links . '");
                $("#mx-threads-total").html('. $sum_all_threads .');
            </script>';

            foreach ($this->_links as $index => $item) {
                if ($item['active'] === true) {

                    $result = $this->_conn->query("SELECT id_msg, id_topic, body FROM {$db_prefix}messages WHERE body REGEXP '" . self::CONFIG['oldUrl'] . "{$item['fromUrl_DB']}' AND id_msg NOT IN ( '" . implode(self::CONFIG['excludeThreads'], "', '") . "' )");


                    //$count_found_links = $result->num_rows;

                    echo '<script>
                        $("#mx-links-process").html("' . ($index + 1) . ' von ' . $count_links . '");
                        $("#mx-threads-process").html('. ($sum_all_threads - $result_num_allrows) .');
                    </script>';
                }




            //        $toUrl = is_string($item['toUrl_PHP']) ? self::CONFIG['newUrl'] . $item['toUrl_PHP'] : '<span class="mx-error">TODO</span>'; // todo
              //



//                    if ($count_found_links > 0) {
/*
                        $li = '<li>' . self::CONFIG['oldUrl'] . $item['fromUrl_DB'] . '<div class="smalltext">&nbsp;&nbsp;-> ' . $toUrl . '<br />&nbsp;&nbsp;' . $this->_('find_threads') . ' (' . $count_found_links . ') -> <span id="mx-span-' . $index . '"></span></div></li>';


                        echo '<script>$("#mx-main-ul").append("' . $li . '");</script>';


                        while ($row = $result->fetch_assoc()) {
                            //echo '<br>Betrag Nr. ' . $row['id_topic'];
                            echo '<script>
                                $("#mx-main-ul").append("' . $row['id_topic'] . '");
                            </script>';


                            $span = '<a href="index.php?topic=' . $row['id_topic'] . '.msg' . $row['id_msg'] . '#msg' . $row['id_msg'] . '" target="_blank">' . $row['id_msg'] . '</a>, ';
                            echo '<script>
                                $("#mx-span-"' . $index . '").append("' . $span . '");
                            </script>';
                        */

//                    }
  //              }
            }

        } else {
            echo 'todo db error';
        }
    }

    private $_conn = null;

    /**
     * Open Database connection.
     *
     * @return bool - Is connection returns true otherwise false.
     * @private
     */
    private function _openDB()
    {
        global $db_server, $db_user, $db_passwd, $db_name;
        $this->_conn = new mysqli($db_server, $db_user, $db_passwd, $db_name);
        if (!$this->_conn) {
            // die( 'Connection failed: ' . mysqli_connect_error() );
            return false;
        }

        // else { echo "Connected successfully"; }
        return true;
    }

    /**
     * Close Database connection.
     *
     * @private
     */
    private function _closeDB()
    {
        if (!is_null($this->_conn)) {
            $this->_conn->close();
        }
        $this->_conn = null;
    }

    function __any()
    {
        /*
        tabelle
        -/-
                            Gesamt,
        Zeilen
        Gefunden
         */


    }

}

new PmxSmf();
